function validateForm() {
    var secA = document.getElementById("passwordInput").value;
    if (secA == "") {
        alert("Es muss ein Passwort eingegeben werden!");
    } else {
        if (validatePassword(secA)) {
            expandDom(secA);
        } else {
            alert("Passwort ist falsch");
        }
    }
}

function validatePassword (password) {
    var mul = 1;
    for (var i = 0; i < password.length; i++) {
        mul = mul * password.charCodeAt(i);
    }
    var hash = mul % 3671; //3671 is a prime number and therefor good for hashing
    if (hash == 28) return true;
    else return false;
}

function expandDom(password) {
    if (!document.getElementById("linkAufwand")) {
        var linkAufwandsbericht = document.createElement("a");
        linkAufwandsbericht.setAttribute("href", "./secure/Aufwanderfassung" + password +  ".xlsx"); // Enter Link
        linkAufwandsbericht.setAttribute("id", "linkAufwand");
        var linkStatusbericht = document.createElement("a");
        linkStatusbericht.setAttribute("href", "./secure/Statusbericht_vom_04-05-20" + password + ".pdf"); // Enter Link

        var h6Aufwandsbericht = document.createElement("h6");
        h6Aufwandsbericht.innerHTML = "Aufwandsbericht";
        var h6Statusbericht = document.createElement("h6");
        h6Statusbericht.innerHTML = "Aktueller Statusbericht";

        linkAufwandsbericht.appendChild(h6Aufwandsbericht);
        linkStatusbericht.appendChild(h6Statusbericht);

        document.getElementById("secureArea").appendChild(linkAufwandsbericht);
        document.getElementById("secureArea").appendChild(linkStatusbericht);
    }
}